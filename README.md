# WebTerminal

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.3.

## Development server
- Modify `app.config.ts` file to point to backend location 
- Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

- Modify `app.config.ts` file to point to backend location 
- Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Deployment

This project can be deployed as static content. It can be deployed in any static content server. For example apache.
To deploy in apache:

- After generating the build, move the dist/webTerminal folder to /var/www/html
- Modify webTerminal/index.html. Set `<base href="#">`
- start apache server

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
