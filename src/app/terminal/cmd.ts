export interface Cmd {
    cmdName: string;
    cmdArgs: Array<string>;
    userName: string;
};