import { AfterViewInit, Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cmd } from './cmd';
import { KernelService } from './kernel.service';

@Component({
  selector: 'app-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.css']
})
export class TerminalComponent implements OnInit, AfterViewInit {

  constructor(private kernelService: KernelService, 
    private route: ActivatedRoute, private router: Router, private elementRef:ElementRef ){
  }

  userName: string = '';
  userHome: string = '';
  terminalIn = "";
  terminalOut = "~$ ";
  cmd: Cmd;
  cw: string;
  cdPath = '.';

  ngOnInit(): void {
    let usr = this.route.snapshot.queryParamMap.get('userName');
    let home = this.route.snapshot.queryParamMap.get('home');
    if (usr && home) {
      this.userName = usr;
      this.userHome = home;
      this.cmd = {cmdName: '', cmdArgs: [], userName: this.userName};
      this.cw = this.userHome;
    } else {
      alert('user not logged, redirecting to login page');
      this.router.navigateByUrl("/login");
    }
  }

  ngAfterViewInit(){
    //this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#fee79a';
    //this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#ffffff'
 }

 submitCmd() {
   if (!this.terminalIn)
    return;
   let cmdParts = this.terminalIn.split(" ");
   this.cmd.cmdName = cmdParts[0].trim();
   let args = [];
   switch (this.cmd.cmdName) {
     case "clear": {
       return this.clearTerm();
     }
     case "pwd": {
       return this.printCW();
     }
     case "echo": {
       cmdParts = this.terminalIn.split("'");
      args.push(cmdParts[1].trim());
      args.push(this.getPath(cmdParts[2].trim()));
       break;
     }
     case "cd": {
       if (!cmdParts[1]){
         this.cdPath = this.userHome;
       } else if (cmdParts[1].trim() == ".") {
         this.cdPath = this.cw;
       } else if (cmdParts[1].trim() == "..") {
         this.cdPath = this.getParent(this.cw);
       } else {
         this.cdPath = this.getPath(cmdParts[1].trim());
       }
       args.push(this.cdPath);
       break;
     }
     case "cat":
     case "mkdir":
     case "rm":
     case "rmdir": {
       args.push(this.getPath(cmdParts[1].trim()));
       break;
     }
     case "ls": {
       let p =  (cmdParts[1]) ? this.getPath(cmdParts[1]) : this.cw;
       args.push(p);
       break;
     }
     case "cp": {
       args.push(this.getPath(cmdParts[1].trim()));
       args.push(cmdParts[2].trim());
       break;
     }
     case "mv": {
       args.push(this.getPath(cmdParts[1].trim()));
       args.push(this.getPath(cmdParts[2].trim()));
     }
     default: {
       // do nothing;
       break;
     }
   }
   this.cmd.cmdArgs = args;
   console.log("command ", this.cmd);
   // do rest call to get output
   this.kernelService.executeCommandInKernel(this.cmd).subscribe(data => {
      if (data) {
        console.log("received data", data);
        if(data['success'] && data['success'] == true) {
          if (this.cmd.cmdName == "cd") {this.cw = this.cdPath;}
          if (data['output']) {
            this.terminalOut += this.terminalIn + "\n" + data['output'] + "\n~$ ";
          } else {
            this.terminalOut += this.terminalIn + "\n~$";
          }
        } else {
          let out =  (data['output']) ? data['output'] : 'problem while processing cmd in kernel'
          this.terminalOut += this.terminalIn + "\nError: " + out + "\n~$ ";
        }
        this.terminalIn = '';
      }
   });
 }
  printCW() {
    this.terminalOut += "pwd\n" + this.cw + "\n~$ ";
    this.terminalIn = '';
  }
  clearTerm() {
    this.terminalOut = "~$ ";
    this.terminalIn = '';
  }

  getParent(path: string): any {
    return path.substring(0, path.lastIndexOf("/"));
  }

  getPath(path: string): any {
    if (!path) {
      return alert("invalid syntax");
    }
    if (path.indexOf("/") == 0) {
      // absolute path detected nothing to do
      return path;
    } else {
      return this.cw + "/" + path;
    }
  }
}
