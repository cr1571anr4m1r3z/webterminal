import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cmd } from './cmd';
import { AppConfig } from '../app.config';

@Injectable({
  providedIn: 'root'
})
export class KernelService {

  constructor(private http:HttpClient) { }

  public executeCommandInKernel(cmd: Cmd) {
      return this.http.post(AppConfig.REST_BASE_URL + "/kernel/executeCmd", cmd);
  }
}
