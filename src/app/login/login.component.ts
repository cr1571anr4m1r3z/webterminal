import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { UserService } from './user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private userService: UserService) { }

  userName=""
  password=""

  ngOnInit(): void {
  }

  register(){
    this.router.navigateByUrl("/register");
  }

  login(){
    let usr = {userName: this.userName, password: this.password};
    this.userService.login(usr).subscribe(res => {
      if (res) {
        if (res['success'] && res['output'] && res['success'] == true) {
          this.router.navigateByUrl('/?userName='+this.userName+'&home='+res['output']);
        } else {
          let msg = 'unable to login';
          if (res['output']) {
            msg = res['output'];
          }
          return alert(msg);
        }
      }
    }, err => {
      console.log("err",err);
      if (err['statusText'] == "OK") {
          alert('user logged in successfully');
          this.router.navigateByUrl('/?userName='+this.userName+'&home=/home/'+this.userName);
      }
    });    
  }

}
