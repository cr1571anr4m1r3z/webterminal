import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConfig } from '../app.config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public registerUser(user: any) {
    return this.http.post(AppConfig.REST_BASE_URL + "/user/register", user);
  }

  public login(user: any) {
    return this.http.post(AppConfig.REST_BASE_URL + "/user/login", user);
  }
}
