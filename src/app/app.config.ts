export class AppConfig {
    public static HTTP_PORT: number = 8081;
    public static HTTP_HOST: string = "localhost";
    public static API_CONTEXT: string = "/os/api/v1"

    public static REST_BASE_URL:string = "http://" + AppConfig.HTTP_HOST + ":" + AppConfig.HTTP_PORT + AppConfig.API_CONTEXT;  
}