import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../login/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  userName=""
  password=""
  confirm=""
  home="/home";

  ngOnInit(): void {
  }

  updateHome(e) {
    this.home = "/home/"+this.userName;
  }
  

  register() {
    if (!this.userName){
      return alert("please enter username");
    }
    if (!this.password) {
      return alert("please specify a password");
    }
    if (this.password != this.confirm) {
      return alert("passwords do not match ");
    }
    let usr = {userName: this.userName, password: this.password, home: this.home};;
    this.userService.registerUser(usr).subscribe(res => {
      if (res) {
        if (res['success'] && res['sucess'] == true) {
          alert('user registered successfully');
          this.router.navigateByUrl('/?userName='+this.userName+'&home='+this.home);
        } else {
          if (res['output']) {
            return alert(res['output']);
          } else {
            return alert('there was a problem while registering your user');
          }
        }
      } else {
        alert("other error");
      }
    }, err => {
      console.log("err",err);
      if (err['statusText'] == "OK") {
          alert('user registered successfully');
          this.router.navigateByUrl('/?userName='+this.userName+'&home='+this.home);
      }
    });
    
  }

}
